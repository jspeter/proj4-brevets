"""
Nosetests for acp_times.py
"""

from acp_times import *

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_0_brevet():
    assert '2017-01-01T00:00:00+00:00' == close_time(0, 200, '2017-01-01T00:00:00+00:00')
    assert '2017-01-01T00:00:00+00:00' == open_time(0, 200, '2017-01-01T00:00:00+00:00')


def test_200_brevet(): 
    assert '2017-01-01T13:30:00+00:00' == close_time(209, 200, '2017-01-01T00:00:00+00:00')
    assert '2017-01-01T05:52:00+00:00' == open_time(209, 200, '2017-01-01T00:00:00+00:00')

def test_above200_brevet():
    assert '2017-01-01T13:20:00+00:00' == close_time(200, 300, '2017-01-01T00:00:00+00:00')
    assert '2017-01-01T05:52:00+00:00' == open_time(200, 300, '2017-01-01T00:00:00+00:00')

def test_above600_brevet():
    assert '2017-01-02T16:00:00+00:00' == close_time(600, 1000, '2017-01-01T00:00:00+00:00')
    assert '2017-01-01T18:47:00+00:00' == open_time(600, 1000, '2017-01-01T00:00:00+00:00')

def test_1000_brevet(): 
    assert '2017-01-02T09:04:00+00:00' == open_time(1009, 1000, '2017-01-01T00:00:00+00:00')
    assert '2017-01-04T03:00:00+00:00' == close_time(1009, 1000, '2017-01-01T00:00:00+00:00')