"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    max_speed = {
      1000: 26,
      600 : 28,
      400 : 30,
      200 : 32,
      0   : 34
    }
    km = control_dist_km
    time = arrow.get(brevet_start_time)
 
    if km > brevet_dist_km:
       km = brevet_dist_km
    for dist in max_speed:
       if km >= dist: 
          diff = km - dist
          km -= diff
          tot = diff / max_speed[dist]
          hrs = int(tot)
          mins = int((tot % 1) * 60)
          time = time.shift(hours=+hrs)
          time = time.shift(minutes=+mins)
    return time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    min_speed = {
       1000: 13.333,
       600 : 11.428,
       0   : 15 
    }
    km = control_dist_km
    time = arrow.get(brevet_start_time)
 
    if km > brevet_dist_km:
       km = brevet_dist_km
   
    if km == 200 & brevet_dist_km == 200:
       time = time.shift(hours=+13)
       time = time.shift(minutes=+30)
    else: 
       for dist in min_speed:
          if km >= dist: 
             diff = km - dist
             km -= diff
             tot = diff / min_speed[dist]
             hrs = int(tot)
             mins = int((tot % 1) * 60)
             time = time.shift(hours=+hrs)
             time = time.shift(minutes=+mins)
    return time.isoformat()
