# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Author

Jake Petersen jpeter17@uoregon.edu

## Brevet Calculation Rules 

Control points open and close times are based on there distance along the route. If a control point is not yet past the brevet calculates time based on table here https://rusa.org/pages/acp-brevet-control-times-calculator.

If a control point is located on a breakpoint use the lower bound of the table to calculate. Ex: Control @ 200km uses 0-200 if brevet is not 200km as well

If a control point is larger than 200 use multiple parts of the table to calculate time Ex: Control @ 500km uses 0-200 for the first 200km, 200-400 for the next 200km, and 400-600 for the last 100km.

if a control point is past the brevet assume it is the last point and assign open and close times based on brevet distance
